# Настройка рабочего места
Для работы с кластером kubernetes требуется скачать бинарник командной строки `kubectl` с сайта `https://kubernetes.io/docs/tasks/tools/`
Выбираем для своей системы.
1. Для - `Windows`
```bash
+ Download the latest release v1.22.0.
+ curl -LO "https://dl.k8s.io/release/v1.22.0/bin/windows/amd64/kubectl.exe"
```
В системе прописываем в переменные среды `системную переменную`
```bash
C:\> setx /M path "%PATH%;C:\path\to\directory\"
+ setx /M path - установка переменных в систему для всех пользователей на постоянной основе
+ %PATH%;C:\path\to\directory\ - Указываем путь к директории где распологается наш файл kubectl 
```
2. для `linux`
Скачиваем архив с kubectl
```bash
+ curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
```
Устанавливаем с указанием владельца:группы 
```bash
sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
```
Проверяем
```bash
kubectl version --client

Client Version: version.Info{Major:"1", Minor:"22", GitVersion:"v1.22.4", GitCommit:"b695d79d4f967c403a96986f1750a35eb75e75f1", GitTreeState:"clean", BuildDate:"2021-11-17T15:48:33Z", GoVersion:"go1.16.10", Compiler:"gc", Platform:"linux/amd64"}
```
Для доступа к кластеру необходим файл с ключами. берем на `master node`
```bash
sudo -s && mkdir -p .kube
scp root@master_node//root/.kube/config /root/.kube/
```

# Создаем deployment
Создаем файл deployment
```bash
vim my-deployment.yaml

apiVersion: apps/v1
kind: Deployment
metadata:
  name: my-deployment
spec:
  replicas: 5
  selector:
    matchLabels:
      app: my-app
  revisionHistoryLimit: 10
  template:
    metadata:
      labels:
        app: my-app
    spec:
      containers:
      - name: firstapp
        image: nginxdemos/hello:0.2
        ports:
        - containerPort: 80
```
Запускаем 
```bash
kubectl apply -f my-deployment.yaml
```
Смотрим в кубе что получилось
Смотрим `Pod`
```bash
kubectl get pod -o wide
NAME                            READY   STATUS    RESTARTS   AGE     IP              NODE    NOMINATED NODE   READINESS GATES
my-deployment-84946fb55-4nlq8   1/1     Running   0          4m22s   10.233.117.42   k8s-3   <none>           <none>
my-deployment-84946fb55-mmjlv   1/1     Running   0          4m22s   10.233.99.39    k8s-2   <none>           <none>
my-deployment-84946fb55-snmxq   1/1     Running   0          4m21s   10.233.117.43   k8s-3   <none>           <none>
my-deployment-84946fb55-vd2jn   1/1     Running   0          4m22s   10.233.117.41   k8s-3   <none>           <none>
my-deployment-84946fb55-zkjcl   1/1     Running   0          4m21s   10.233.99.38    k8s-2   <none>           <none>
```
Смотрим `deployment`
```bash
kubectl get deploy -o wide
NAME            READY   UP-TO-DATE   AVAILABLE   AGE     CONTAINERS   IMAGES                 SELECTOR
my-deployment   5/5     5            5           4m56s   firstapp     nginxdemos/hello:0.2   app=my-app
```
Смотрим `replicaset`
```bash
kubectl get rs -o wide
NAME                      DESIRED   CURRENT   READY   AGE     CONTAINERS   IMAGES                 SELECTOR
my-deployment-84946fb55   5         5         5       6m33s   firstapp     nginxdemos/hello:0.2   app=my-app,pod-template-hash=84946fb55
```
# Разворачиваем сервисы
## Первый сервис `ClusterIP`
Создаем файлы с сервисами
1. Файл my-svc-clusterip.yaml
```bash
vim my-svc.clusterip.yaml

apiVersion: v1
kind: Service
metadata:
  name: my-svc-clusterip
spec:
  selector:
    app: my-app
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80
```
Применяем файл
```bash
kubectl apply -f my-svc-clusterip.yaml
```
Проверяем что получилось
```bash
kubectl get svc -o wide
NAME               TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)   AGE     SELECTOR
kubernetes         ClusterIP   10.233.0.1      <none>        443/TCP   5d22h   <none>
my-svc-clusterip   ClusterIP   10.233.47.158   <none>        80/TCP    2m17s   app=my-app
```
Как видим сервис создался и имеет `Cluster-IP`. Теперь можем проверить доступ по `Cluster-IP` с локальной машины. Для этого необходимо запустить `port-forward`.
Запускаем `port-forward`
```bash
kubectl port-forward svc/my-svc-clusterip 80:80
```
Проверяем. Открываем браузер и вводим `localhost`.
Получаем
```bash
Server address:	127.0.0.1:80
Server name:	my-deployment-84946fb55-mmjlv
Date:	05/Dec/2021:15:57:57 +0000
URI:	/
```
Сравниваем имена подов
```bash
kubectl get pod -o wide
NAME                            READY   STATUS    RESTARTS   AGE   IP              NODE    NOMINATED NODE   READINESS GATES
my-deployment-84946fb55-4nlq8   1/1     Running   0          29m   10.233.117.42   k8s-3   <none>           <none>
my-deployment-84946fb55-mmjlv   1/1     Running   0          29m   10.233.99.39    k8s-2   <none>           <none>
my-deployment-84946fb55-snmxq   1/1     Running   0          29m   10.233.117.43   k8s-3   <none>           <none>
my-deployment-84946fb55-vd2jn   1/1     Running   0          29m   10.233.117.41   k8s-3   <none>           <none>
my-deployment-84946fb55-zkjcl   1/1     Running   0          29m   10.233.99.38    k8s-2   <none>           <none>
```
Видим что нам `nginx` отдал имя нашего пода, а именно `my-deployment-84946fb55-mmjlv`

## Второй сервис `NodePort`
Создаем файл `my-svd-nodeport.yaml`
```bash
vim my-svc-nodeport.yaml

apiVersion: v1
kind: Service
metadata:
  name: my-svc-nodeport
spec:
  selector:
    app: my-app
  type: NodePort
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80
      nodePort: 30000
```
Тут указали в спецификации `spec`

+ Тип `type: NodePort`
+ B `ports` тип `nodePort: 30000`

Тем самым указав что на каждой `node` открыть 30000 порт и сопоставить с портом 80 нашего контейнера.
Применяем файл `kubectl apply -f my-svc-nodeport`.
Проверяем
```bash
kubectl get svc -o wide
NAME               TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)        AGE     SELECTOR
kubernetes         ClusterIP   10.233.0.1      <none>        443/TCP        5d23h   <none>
my-svc-clusterip   ClusterIP   10.233.47.158   <none>        80/TCP         50m     app=my-app
my-svc-nodeport    NodePort    10.233.48.151   <none>        80:30000/TCP   6s      app=my-app
```
Видим `NAME - my-svc-nodeport`, `TYPE - NodePort`, `PORT(S) - 80:30000`.
Смотрим IP адреса наших `Worker node`
```bash
kubectl get nodes -o wide
NAME    STATUS   ROLES                  AGE     VERSION   INTERNAL-IP     EXTERNAL-IP   OS-IMAGE             KERNEL-VERSION       CONTAINER-RUNTIME
k8s-1   Ready    control-plane,master   5d23h   v1.21.6   172.16.10.191   <none>        Ubuntu 18.04.6 LTS   4.15.0-163-generic   docker://20.10.8
k8s-2   Ready    <none>                 5d23h   v1.21.6   172.16.10.192   <none>        Ubuntu 18.04.6 LTS   4.15.0-163-generic   docker://20.10.8
k8s-3   Ready    <none>                 5d23h   v1.21.6   172.16.10.193   <none>        Ubuntu 18.04.6 LTS   4.15.0-163-generic   docker://20.10.8
```
Берем любой IP адрес `Worker node` и подставляем в браузер вместо `localhost` c указанным портом. Как пример `172.16.10.192:30000`
В браузере должны увидеть картинку
```bash
Server address:	10.233.99.39:80
Server name:	my-deployment-84946fb55-mmjlv
Date:	05/Dec/2021:16:46:46 +0000
URI:	/
```
Проверим что именно этот под запущен на этой `Worker node`
```bash
 kubectl get pod -o wide
NAME                            READY   STATUS    RESTARTS   AGE   IP              NODE    NOMINATED NODE   READINESS GATES
...
my-deployment-84946fb55-mmjlv   1/1     Running   0          69m   10.233.99.39    k8s-2   <none>           <none>
...
```
Все отлично, все работает.

# Задание со звездочкой *
Прикручиваем пробы.
Имеются 3 пробы для проверки.

+ `1. readinessProbe`

  + 1.1. Kublet использует пробу для проверки готовности пода с приложением принимать и обрабатывать трафик. Иногда приложению требуется время что бы обрабатывать трафик. К примеру провести миграции в базе или загрузить статические страницы сайта. На примере `nginx`.
```bash
  readinessProbe:
    failureThreshold: 3 - опция задает колличество попыток с ошибками. Если получаем 3 провальные перезапускаем под. По умолчанию 3.
    httpGet:  - тип пробы. Есть 4 вида проверки. httpGet, tcpSocket, grpc, exec. Для nginx используем hhtpGet
      path: / - запрос в корень. Ожидаем ответ с кодом 200
      port: 80  - запрос по порту (http)
    periodSeconds: 10 - Как часто (в секундах) выполнять запрос. По умолчанию 10 сек.
    successThreshold: 1 - Колличество успешных попыток. по умолчанию 1.
    timeoutSeconds: 1 - Количество секунд ожидания между запросами. По умолчанию 1 сек.
```

+ `2. livenessProbe`

  + 2.1. Kublet выполняет проверку приложения. Отправляет запрос в контейнер и ожидает код возврата. В данном случае выполняет проверку приложения (`nginx`) по `http` и ожидает получение кода `200 - 399`. Любой их этих кодов означает, что приложение работает.
```bash
  livenessProbe:
    failureThreshold: 3
    httpGet:
      path: /
      port: 80
    periodSeconds: 10
    successThreshold: 1
    timeoutSeconds: 1
    initialDelaySeconds: 10 - Через какой промежуток времени после запуска проводить пробу.
```
Смотрим наши `pod`
```bash
kubectl get pod -o wide
NAME                            READY   STATUS    RESTARTS   AGE   IP              NODE    NOMINATED NODE   READINESS GATES
my-deployment-84946fb55-4nlq8   1/1     Running   0          16m   10.233.117.42   k8s-3   <none>           <none>
my-deployment-84946fb55-mmjlv   1/1     Running   0          16m   10.233.99.39    k8s-2   <none>           <none>
my-deployment-84946fb55-snmxq   1/1     Running   0          16m   10.233.117.43   k8s-3   <none>           <none>
my-deployment-84946fb55-vd2jn   1/1     Running   0          16m   10.233.117.41   k8s-3   <none>           <none>
my-deployment-84946fb55-zkjcl   1/1     Running   0          16m   10.233.99.38    k8s-2   <none>           <none>
```
Смотрим `log` любого контейнера
```bash
kubectl logs my-deployment-799bdffdb9-5b5g2
/docker-entrypoint.sh: /docker-entrypoint.d/ is not empty, will attempt to perform configuration
/docker-entrypoint.sh: Looking for shell scripts in /docker-entrypoint.d/
/docker-entrypoint.sh: Launching /docker-entrypoint.d/10-listen-on-ipv6-by-default.sh
10-listen-on-ipv6-by-default.sh: info: /etc/nginx/conf.d/default.conf is not a file or does not exist
/docker-entrypoint.sh: Launching /docker-entrypoint.d/20-envsubst-on-templates.sh
/docker-entrypoint.sh: Launching /docker-entrypoint.d/30-tune-worker-processes.sh
/docker-entrypoint.sh: Configuration complete; ready for start up
2021/12/05 16:52:34 [notice] 1#1: using the "epoll" event method
2021/12/05 16:52:34 [notice] 1#1: nginx/1.21.4
2021/12/05 16:52:34 [notice] 1#1: built by gcc 10.3.1 20210424 (Alpine 10.3.1_git20210424)
2021/12/05 16:52:34 [notice] 1#1: OS: Linux 4.15.0-163-generic
2021/12/05 16:52:34 [notice] 1#1: getrlimit(RLIMIT_NOFILE): 1048576:1048576
2021/12/05 16:52:34 [notice] 1#1: start worker processes
2021/12/05 16:52:34 [notice] 1#1: start worker process 24
2021/12/05 16:52:34 [notice] 1#1: start worker process 25
2021/12/05 16:52:34 [notice] 1#1: start worker process 26
2021/12/05 16:52:34 [notice] 1#1: start worker process 27
172.16.10.193 - - [05/Dec/2021:16:52:39 +0000] "GET / HTTP/1.1" 200 7252 "-" "kube-probe/1.21" "-"
172.16.10.193 - - [05/Dec/2021:16:52:49 +0000] "GET / HTTP/1.1" 200 7252 "-" "kube-probe/1.21" "-"
172.16.10.193 - - [05/Dec/2021:16:52:49 +0000] "GET / HTTP/1.1" 200 7252 "-" "kube-probe/1.21" "-"
172.16.10.193 - - [05/Dec/2021:16:52:59 +0000] "GET / HTTP/1.1" 200 7252 "-" "kube-probe/1.21" "-"
172.16.10.193 - - [05/Dec/2021:16:52:59 +0000] "GET / HTTP/1.1" 200 7252 "-" "kube-probe/1.21" "-"
172.16.10.193 - - [05/Dec/2021:16:53:09 +0000] "GET / HTTP/1.1" 200 7252 "-" "kube-probe/1.21" "-"
172.16.10.193 - - [05/Dec/2021:16:53:09 +0000] "GET / HTTP/1.1" 200 7252 "-" "kube-probe/1.21" "-"
```
Видим что каждые 10 секунд `livenessProbe` запрашивает состояние приложения. получает код 200.

+ `3. startupProbe`

Ожидает запуск приложения. Работает только при старте приложения. И только после того как `startupProbe` отработает, запустятся `readinessProbe` и `livenessProbe`
```bash
  startupProbe:
    httpGet:
      path: /
      port: 80
    failureThreshold: 30  - Колличество проверок. По умолчанию 30.
    periodSeconds: 10 - Время повторения проверки. По умолчанию 10 сек.
```
Отсюда следует, что если приложение не ответило в течении 5 минут ```(30 * 10 сек = 300 сек / 60 = 5 миунт) ```. то выполняется действие согластно политики ```restartPolicy```

Своими словами. 
```startupProbe``` ожидает запуск приложения. То есть, под должен загрузиться и запуститься. только после этого будут запущены остальные пробы.
```readinessProbe``` проверяет готовность приложения внутри пода к приему и обработки трафика. Приложение может быть тяжелым или зависить от других приложений. В данном случае, проба проверяет порт 80 методом httpGet и ждет получение кода 200 или 3хх. Если получен такой код. значит приложение готово к работе и приему трафика.
```livenessProbe``` выполняется постоянно через заданный промежуток времени. Оно следит за приложением весь срок его работы (жизни). В данном случает так же используется httpGet и ждет получение кода 200 или 3хх. 

----
+ ### К оглавлению! [___Оглавление___](../README.md)