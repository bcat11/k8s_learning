# Вариант 1.
+ Переменные можно внести на живую.
```yaml
kubectl get deployment  # Запрашиваем deployment
```
вывод:
```bash
NAME            READY   UP-TO-DATE   AVAILABLE   AGE
my-deployment   2/2     2            2           134m
```
Вносим env на живую. Делаем правки используя `kubectl edit`.
```yaml
kubectl edit deploy my-deployment
```
Интересует секция `spec.containers`. 
```bash
... # Описание создаваемого контейнера
    spec:
      containers:
        image: nginxdemos/hello:0.2
        imagePullPolicy: IfNotPresent
        livenessProbe:
          failureThreshold: 3
          httpGet:
            path: /
            port: 80
            scheme: HTTP
          initialDelaySeconds: 10
          periodSeconds: 10
          successThreshold: 1
          timeoutSeconds: 1
        name: firstapp
        ports:
        - containerPort: 80
          protocol: TCP
        readinessProbe:
          failureThreshold: 3
          httpGet:
            path: /
            port: 80
            scheme: HTTP
          periodSeconds: 10
          successThreshold: 1
          timeoutSeconds: 1
        resources: {}
        startupProbe:
          failureThreshold: 30
          httpGet:
            path: /
            port: 80
            scheme: HTTP
          periodSeconds: 10
          successThreshold: 1
          timeoutSeconds: 1
...
```
Редактируем deployment на лету.
```yaml
[...]
    spec:
      containers:
--- # Начало секции с переменными 
      - env:                      # Определяем переменные
        - name: ONE               # Задаем имя переменной
          value: My_test_value    # Задаем значение переменной
--- # Конец секции с переменными
        image: nginxdemos/hello:0.2
[...]
```
Добавление секции с переменными (в данном случае одна переменная с именем `"name: ONE"` и значением `"value: My_test_value"` ) в файл deployment, присвоит переменную для всех имеющихся подов и вызовит процедуру обновления. То есть, запустит 1 новый под, затем прибьет 1 старый под. Далее запустит 2 новый под, и прибьет 2 старый под. Такая процедура задается стратегией обновления. Стратегия обновления имеет 2 варианта обновления.
+ `RollingUpdate` - (Сначала поднимает 25% подов с новой конфигурацией, и удаляет 25% со старой конфигурацией)
+ `ReCreate` - (Удаляет поды со старой конфигурацией и поднимает поды с новой конфигурацией)
  
После сохранения и обновления подов посмотрим `deployment`.
```yaml
kubectl describe deploy my-deployment # Делаем describe deploymet
```
Вывод. интересует секция `Pod Template`
```bash
Pod Template:
  Labels:       app=my-app
  Annotations:  kubectl.kubernetes.io/restartedAt: 2022-01-04T12:48:00+03:00
  Containers:
   firstapp:
    Image:      nginxdemos/hello:0.2
    Port:       80/TCP
    Host Port:  0/TCP
    Liveness:   http-get http://:80/ delay=10s timeout=1s period=10s #success=1 #failure=3
    Readiness:  http-get http://:80/ delay=0s timeout=1s period=10s #success=1 #failure=3
    Startup:    http-get http://:80/ delay=0s timeout=1s period=10s #success=1 #failure=30
--- # Имеем переменную
    Environment:
      ONE:   My_test_value # Имя переменной "ONE" и ее значение "My_test_value"
--- # Конец
    Mounts:  <none>
  Volumes:   <none>
Conditions:
```
Посмотрим в сами pod`ы.
```yaml
kubectl get pod
```
```yaml
NAME                             READY   STATUS    RESTARTS   AGE
my-deployment-76d4b84c75-cf6rw   1/1     Running   0          43m
my-deployment-76d4b84c75-k8wjq   1/1     Running   0          43m
```
Выберем любой `pod` и посмотрим переменные.
```yaml
kubectl exec -ti my-deployment-76d4b84c75-k8wjq -- env
```
В выводе должны увидеть список переменных.
```yaml
[...]
  ONE=My_test_value   # Наша переменная
[...]
```
------
***Примечание.***

Данный вариант желательно не использовать, а передавать переменные через `ConfigMap`

----
+ ### К оглавлению! [___Оглавление___](./README.md)