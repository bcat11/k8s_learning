# Два контейнера в поде

Условия задачи:
```bash
Добавить в наш существующий Deployment 2й контейнер, повесить на него пробы.
Научиться входить в 2 разных контейнера с помощью kubectl exec.
```
-----

Для начала очистим наш кластер, что бы предыдущие эксперемены не мешали.

Очистим командой 
```bash
kubectl delete -f . # Выполняем в тойже директории откуда и запускали все файлы. поскольку . (точка) говорит нам удалить все абстракции из файлов yaml
```
Все будем делать с нуля. для более лучшего закрепления материала.

Создадим наш простой `deployment`.
```bash 
vim two_conyainers.yaml # название файла
[...]
# Минимальный deployment

---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: two-containers                # Задаем имя deployment
  namespace: default                  # Задаем namespace
  labels:                             # Навешиваем метки
    app: two-containers               
    tier: nginx
spec:
  replicas: 2
  selector:
    matchLabels:
      tier: nginx
  revisionHistoryLimit: 10
  template:
    metadata:
      labels:
        tier: nginx
    spec:
      containers:
      - name: nginx-one                 # Задаем имя первого контейнера
        image: nginxdemos/hello:0.2
        ports:
        - containerPort: 80             # Описание номера порта на котором будет слушать первый контейнер
      - name: nginx-two                 # Задаем имя второго контейнера
        image: nginxdemos/hello:0.2
        ports:
        - containerPort: 81             # Описание номера порта на котором будет слушать второй контейнер

```
Теперь необходимо создать `configmap` с конфигурацией наших контейнеров.
```bash
vim two-configmap.yaml
[....]
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: two-configmap
  namespace: default
  labels:
    app: two-containers
    tier: nginx
data:
  nginx-one.conf: |                   # Конфиг для первого контейнера (nginx-one)
    server {
      listen       80 default_server; # Указываем какой слушать порт
      server_name  _;
      default_type text/plain;

      root /usr/share/nginx/html;
      try_files /index.html =404;

      expires -1;

      sub_filter_once off;
      sub_filter 'server_hostname' '$hostname';
      sub_filter 'server_address' '$server_addr:$server_port';
      sub_filter 'server_url' '$request_uri';
      sub_filter 'server_date' '$time_local';
      sub_filter 'request_id' '$request_id';
    }
  nginx-two.conf: |                   # Конфиг для второго контейнера (nginx-two)
    server {
      listen       81 default_server; # Указываем какой слушать порт
      server_name  _;
      default_type text/plain;

      root /usr/share/nginx/html;
      try_files /index.html =404;

      expires -1;

      sub_filter_once off;
      sub_filter 'server_hostname' '$hostname';
      sub_filter 'server_address' '$server_addr:$server_port';
      sub_filter 'server_url' '$request_uri';
      sub_filter 'server_date' '$time_local';
      sub_filter 'request_id' '$request_id';
    }
```
    * В рамках одного пода все контейнеры имеют общее сетевое пространство и не могут слушать один и тот же порт на localhost. По этому требуется назначить разные порты для каждого контейнера.

Подключаем наш `configmap` к `deployment`. Для каждого контейнера описываем `volumeMounts`, а так же описываем сам том `volumes`. 

В секции `spec.containers` описываем точки монтирования томов.
```yaml
# Для первого контейнера
[...]
  volumeMounts:                             # Здесь мы описываем точки монтирования томов внутри контейнера
  - name: config                            # Указываем имя тома для монтирования
    mountPath: /etc/nginx/conf.d/hello.conf # Здесь мы указываем точку монтирования. Копируем конфиг из configmap в файл hello.conf
    subPath: nginx-one.conf
[...]
# Для второго контейнера
[...]
  volumeMounts:
  - name: config
    mountPath: /etc/nginx/conf.d/hello.conf
    subPath: nginx-two.conf
[...]
    * стоит обратить внимание на subPath - данной дерективой мы читаем часть конфига из configmap`a относящегося именно к этому контейнеру
# И наконец описываем сам том.
[...]
  volumes:                          # Здесь (на уровень выше!) мы описываем тома
  - name: config                    # Задаём имя тому
    configMap:                      # Указываем, из какого конфигмапа создать том
      name: two-configmap
[...]
```
Тестируем наш `configmap` и `deployment`. Применяем их командами:
```bash
kubectl apply -f two-configmap.yaml   # Сначала применяем configmap
kubectl apply -f two-containers.yaml  # Затем применяем deployment
```
Смотрим что происходит
```bash
kubectl get pod -w
```
В выводе должны увидеть нечто подобное:
```yaml
NAME                              READY   STATUS              RESTARTS   AGE
two-containers-78d947d746-8w6fl   1/1     Running             0          13m
two-containers-78d947d746-v5b9v   1/1     Running             0          14m
two-containers-f4695698c-t24p2    0/2     ContainerCreating   0          6s
two-containers-f4695698c-t24p2    2/2     Running             0          11s
two-containers-78d947d746-8w6fl   1/1     Terminating         0          13m
two-containers-f4695698c-7mqtg    0/2     Pending             0          0s
two-containers-f4695698c-7mqtg    0/2     Pending             0          0s
two-containers-f4695698c-7mqtg    0/2     ContainerCreating   0          0s
two-containers-78d947d746-8w6fl   1/1     Terminating         0          14m
two-containers-f4695698c-7mqtg    0/2     ContainerCreating   0          5s
two-containers-78d947d746-8w6fl   0/1     Terminating         0          14m
two-containers-78d947d746-8w6fl   0/1     Terminating         0          14m
two-containers-78d947d746-8w6fl   0/1     Terminating         0          14m
two-containers-f4695698c-7mqtg    2/2     Running             0          10s
two-containers-78d947d746-v5b9v   1/1     Terminating         0          14m
two-containers-78d947d746-v5b9v   1/1     Terminating         0          14m
two-containers-78d947d746-v5b9v   0/1     Terminating         0          14m
two-containers-78d947d746-v5b9v   0/1     Terminating         0          14m
two-containers-78d947d746-v5b9v   0/1     Terminating         0          14m
```
Данный вывод говорит нам, что наш `deployment` применился и `kubernetes` пересоздал наши поды. Обрашаем внимание на `READY`. У вновь созданных подов должно быть 2/2. Это говорит нам о том, что в одном нашем поде имеется два контейнера. Посмотрим `describe` пода.
```bash
kubectl describe pod two-containers-f4695698c-7mqtg
```
В секции `Containers:` должны увидеть описание наших контейнеров
```yaml
Containers:
  nginx-one:                  # Первый контейнер
    Container ID:   docker://57989b846c729160bf5453b63c0cecc8b2dbc05c873a04e4a0ab471e24a68cd1
    Image:          nginxdemos/hello:0.2
    Image ID:       docker-pullable://nginxdemos/hello@sha256:f0ca0e04007f7c8e4b9d2431ea16d41f1bbdeb290c2d7427ee2f3bc5b433474f
    Port:           80/TCP    # Порт который слушает контейнер
    Host Port:      0/TCP
    State:          Running
      Started:      Wed, 05 Jan 2022 13:02:03 +0300
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /etc/nginx/conf.d/hello.conf from config (rw,path="nginx-one.conf")       # Конфиг нашего nginx из configmap 
      /var/run/secrets/kubernetes.io/serviceaccount from kube-api-access-8w6fd (ro)
  nginx-two:                    # Второй контейнер
    Container ID:   docker://ed3be19bed9c8dcbdda1fc9d8af453e36652b2a55d8d45dffc9f4ed7f05263b0
    Image:          nginxdemos/hello:0.2
    Image ID:       docker-pullable://nginxdemos/hello@sha256:f0ca0e04007f7c8e4b9d2431ea16d41f1bbdeb290c2d7427ee2f3bc5b433474f
    Port:           81/TCP      # Порт который слушает конейнер
    Host Port:      0/TCP
    State:          Running
      Started:      Wed, 05 Jan 2022 13:02:05 +0300
    Ready:          True
    Restart Count:  0
    Limits:
      cpu:     200m
      memory:  512Mi
    Requests:
      cpu:        100m
      memory:     256Mi
    Environment:  <none>
    Mounts:
      /etc/nginx/conf.d/hello.conf from config (rw,path="nginx-two.conf")        # Конфиг нашего nginx из configmap
      /var/run/secrets/kubernetes.io/serviceaccount from kube-api-access-8w6fd (ro)
Conditions:
```
Тут можем увидить, что в нашем поде имеется два контейнера.

Как работать с такими подами/контейнерами? Давайте посмотрим логи одного из контейнера в поде
```bash
kubectl logs two-containers-f4695698c-7mqtg -c nginx-one
```
    * # Тут к имени пода добавляется -c и имя контейнера в поде
И мы получаем следующий вывод
```yaml
/docker-entrypoint.sh: /docker-entrypoint.d/ is not empty, will attempt to perform configuration
/docker-entrypoint.sh: Looking for shell scripts in /docker-entrypoint.d/
/docker-entrypoint.sh: Launching /docker-entrypoint.d/10-listen-on-ipv6-by-default.sh
10-listen-on-ipv6-by-default.sh: info: /etc/nginx/conf.d/default.conf is not a file or does not exist
/docker-entrypoint.sh: Launching /docker-entrypoint.d/20-envsubst-on-templates.sh
/docker-entrypoint.sh: Launching /docker-entrypoint.d/30-tune-worker-processes.sh
/docker-entrypoint.sh: Configuration complete; ready for start up
2022/01/05 12:21:56 [notice] 1#1: using the "epoll" event method
2022/01/05 12:21:56 [notice] 1#1: nginx/1.21.4
2022/01/05 12:21:56 [notice] 1#1: built by gcc 10.3.1 20210424 (Alpine 10.3.1_git20210424)
2022/01/05 12:21:56 [notice] 1#1: OS: Linux 4.15.0-163-generic
2022/01/05 12:21:56 [notice] 1#1: getrlimit(RLIMIT_NOFILE): 1048576:1048576
2022/01/05 12:21:56 [notice] 1#1: start worker processes
[...]
```
Попробуем зайти в контейнер и прочитать конфиг файл `nginx`
```bash
# Читаем конфиг первого контейнера
kubectl exec -ti two-containers-76b9d5c7d5-g7nf8 -c nginx-one -- cat /etc/nginx/conf.d/hello.conf
[...]
server {
    listen       80 default_server;
    server_name  _;
    default_type text/plain;

    root /usr/share/nginx/html;
    try_files /index.html =404;

    expires -1;

    sub_filter_once off;
    sub_filter 'server_hostname' '$hostname';
    sub_filter 'server_address' '$server_addr:$server_port';
    sub_filter 'server_url' '$request_uri';
    sub_filter 'server_date' '$time_local';
    sub_filter 'request_id' '$request_id';
}
[...]
# Читаем конфиг второго контейнера
kubectl exec -ti two-containers-76b9d5c7d5-g7nf8 -c nginx-two -- cat /etc/nginx/conf.d/hello.conf
[...]
server {
    listen       81 default_server;
    server_name  _;
    default_type text/plain;

    root /usr/share/nginx/html;
    try_files /index.html =404;

    expires -1;

    sub_filter_once off;
    sub_filter 'server_hostname' '$hostname';
    sub_filter 'server_address' '$server_addr:$server_port';
    sub_filter 'server_url' '$request_uri';
    sub_filter 'server_date' '$time_local';
    sub_filter 'request_id' '$request_id';
}
```
Как видим, для работы с отдельным контейнером в поде достаточно добавить опцию -c имя_контейнера. Таким образом мы прочитали конфиги из наших контейнеров запущенных в одном поде.

Теперь попробуем сделать `port-forward` на наши контейнеры
```bash
kubectl port-forward two-containers-76b9d5c7d5-g7nf8 8181:80
[...]
Forwarding from 127.0.0.1:8181 -> 80
Forwarding from [::1]:8181 -> 80
```
Обращаемся `curl` на порт 8181. Вывод:
```yaml
[...]
<div class="info">
<p><span>Server&nbsp;address:</span> <span>127.0.0.1:80</span></p>
<p><span>Server&nbsp;name:</span> <span>two-containers-76b9d5c7d5-g7nf8</span></p>
<p class="smaller"><span>Date:</span> <span>05/Jan/2022:14:42:18 +0000</span></p>
<p class="smaller"><span>URI:</span> <span>/</span></p>
</div>
<div class="check"><input type="checkbox" id="check" onchange="changeCookie()"> Auto Refresh</div>
    <div id="footer">
        <div id="center" align="center">
            Request ID: cbff4fb55cce5020c96c6c51fe3ec51f<br/>
            &copy; NGINX, Inc. 2018
        </div>
    </div>
</body>
</html>
```
Тут мы видим что нам отвечает контейнер `nginx` запущенный на 80 порту, а именно контейнер с именем `nginx-one`. Что бы получить информацию из второго контейнера, сделаем порт форвардинг на другой порт, а именно на 81.
```bash
kubectl port-forward two-containers-76b9d5c7d5-g7nf8 8181:81
[...]
Forwarding from 127.0.0.1:8181 -> 81
Forwarding from [::1]:8181 -> 81
```
Обращаемся `curl` на порт 8181. Вывод:
```yaml
[...]
<div class="info">
<p><span>Server&nbsp;address:</span> <span>127.0.0.1:81</span></p>
<p><span>Server&nbsp;name:</span> <span>two-containers-76b9d5c7d5-g7nf8</span></p>
<p class="smaller"><span>Date:</span> <span>05/Jan/2022:14:48:36 +0000</span></p>
<p class="smaller"><span>URI:</span> <span>/</span></p>
</div>
<div class="check"><input type="checkbox" id="check" onchange="changeCookie()"> Auto Refresh</div>
    <div id="footer">
        <div id="center" align="center">
            Request ID: 76b8f5bae417adb59791c17916e90270<br/>
            &copy; NGINX, Inc. 2018
        </div>
    </div>
</body>
</html>
```
И видим что нам отвечает контейнер `nginx` запущенный на 81 порту. То есть нам отвечает контейнер `nginx-two`

Теперь добавим пробы для каждого контейнера. Дополняем наш `deployment` строками
```yaml
readinessProbe:
  httpGet:
    path: /
    port: # Для первого контейнера порт будет 80, для второго 81
  initialDelaySeconds: 10     # Количество секунд от старта контейнера до начала проб
  timeoutSeconds: 1           # Количество секунд ожидания пробы
  successThreshold: 1         # Минимальное количество последовательных проверок, чтобы проба считалась успешной после неудачной
  failureThreshold: 3         # Колличество попыток проведения проверки. Если ошибка то помечаем Unready
livenessProbe:
  httpGet:
    path: /
    port: # Для первого контейнера порт будет 80, для второго 81
  initialDelaySeconds: 10     # Количество секунд от старта контейнера до начала проб
  timeoutSeconds: 1           # Количество секунд ожидания пробы
  successThreshold: 1         # Минимальное количество последовательных проверок, чтобы проба считалась успешной после неудачной
  failureThreshold: 3         # Колличество попыток проведения проверки. Если ошибка 3 раза перезапуск контейнера
```
Так же я добавил ограничения по использованию ресурсов
```yaml
resources:
  requests:
    memory: 256Mi # Запрашиваемое кол-во памяти
    cpu: 100m     # Запрашиваемое кол-во милицпу 1cpu/1000=1m. Можно указать целое значение 1 без m, тогда приложению будет зарезервирован 1 ядро процессора
  limits:
    memory: 512Mi # Максимальное кол-во памяти что может использовать приложение
    cpu: 200m     # Максимальное кол-во CPU что может использовать приложение
```

----
+ ### К оглавлению! [___Оглавление___](./README.md)